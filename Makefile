MASM_PATH=/home/george/progs/masm

AS=$(MASM_PATH)/bin/asl
P2BIN=$(MASM_PATH)/bin/p2bin
MHMT=$(MASM_PATH)/bin/mhmt
CSUM32=$(MASM_PATH)/bin/csum32

AS_FLAGS= -i include -i src -U -L

TARGET=hdd_boot.$$C hdd_boot.scl

all: $(TARGET)

hdd_boot.scl: obj/make_scl_evo.rom obj/csum32.bin
	cat $^ >$@

hdd_boot.$$C: src/make_hobeta_evo.p
	$(P2BIN) $< hdd_boot.\$$C -r \$$-\$$ -k

src/make_hobeta_evo.p: lst obj obj/hdd_boot.rom src/make_hobeta_evo.a80
	$(AS) $(AS_FLAGS) src/make_hobeta_evo.a80 -olist lst/$(subst .p,.lst,$(notdir $@))

obj/make_scl_evo.rom: lst obj obj/main_evo_pack.rom

obj/main_evo_pack.rom: obj/hdd_boot.rom
	$(MHMT) -mlz $< $@

obj/csum32.bin: obj/make_scl_evo.rom
	$(CSUM32) $<
	mv csum32.bin $(dir $@)

lst:
	mkdir $@
obj:
	mkdir $@

src/%.p : src/%.a80
	$(AS) $(AS_FLAGS) $< -olist lst/$(subst .p,.lst,$(notdir $@))

obj/%.rom : src/%.p
	$(P2BIN) $< $@ -r \$$-\$$ -k

%.$$C : src/%.p
	$(P2BIN) $< $@ -r \$$-\$$ -k


clean:
	rm -rfv lst obj hdd_boot.scl hdd_boot.\$$C